class Test {
    public static void main(String[] args) {
        int a;
        int b;
        a = 1;
        b = 2;
        if (a >= b) {
            System.out.println(a + " is greater than or equal to " + b);
        } else {
            System.out.println(a + " is less than " + b);
        }
    }
}