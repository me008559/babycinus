class TestBugJ2 {
    public static void main(String[] args) {
        int x;
        int y;
        x = 1;
        y = 2;
        x += y;
        //If this prints out 2, the bug exists; it should be 3 otherwise.
        System.out.println(x); 
    }
}
