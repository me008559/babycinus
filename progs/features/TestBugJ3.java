class TestBugJ3 {
    public static void main(String[] args) {
        int x;
        int y;
        x = 1;
        y = 2;
        x += y; 

        //If this prints 1, the original value of x, the bug exists; it should be 3 otherwise.
        System.out.println(x);
    }
}
